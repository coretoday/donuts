package com.core.today.prism;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONObject;

import com.example.core.today.R;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.StrictMode;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class TimeLine extends Activity {
    /** Called when the activity is first created. */

    private LinearLayout mPickDate;
    Context context = this;
    private int mYear;
    private int mMonth;
    private int mDay;
    private String year;
    private String month;
    private String day;
    private String YTD_Cur;
    private Boolean isStart = false;
    private String sample;
   private ArrayList<String> arraylist = new ArrayList<String>();
   private ArrayList<Integer> count_array = new ArrayList<Integer>();
    private ArrayAdapter<String> adapter;
//    private ArrayList<TimeLineItem> arraylist;
//    private TimeLineNewsAdapter adapter;
    static final int DATE_DIALOG_ID = 0;
    
    private TextView mDateDisplay;
	private TextView mYearDisplay;
	private TextView mMonthDisplay;
    private ImageView yesterday;
    private ImageView tomorrow;
    private String date;
    Date mdate;
    
    static int count =0;

    private ListView list;

    Calendar calendar = new GregorianCalendar(Locale.KOREA);
    @SuppressLint("NewApi")
	@Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.timeline);
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "Pacifico.ttf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("Prism");
		getActionBar().setDisplayOptions(actionBar.DISPLAY_HOME_AS_UP | actionBar.DISPLAY_SHOW_HOME | actionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
        if(android.os.Build.VERSION.SDK_INT > 9) {
 
            StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();

                StrictMode.setThreadPolicy(policy);

        }
        // capture our View elements
        yesterday = (ImageView) findViewById(R.id.yesterday);
        tomorrow = (ImageView) findViewById(R.id.tomorrow);
        
        mDateDisplay = (TextView) findViewById(R.id.date);
        mYearDisplay = (TextView) findViewById(R.id.year);
        mMonthDisplay= (TextView) findViewById(R.id.month);
        mPickDate = (LinearLayout) findViewById(R.id.pickDate);
        list = (ListView) findViewById(R.id.list);

        tomorrow.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, 1);
		        year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
		        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        if(date.compareTo(YTD_Cur) > 0)
		        		Toast.makeText(context, "기록 정보가 없습니다.", 500).show();
		        else{

		        mDateDisplay.setText(day);
			    mYearDisplay.setText(year);
			    mMonthDisplay.setText(month);
//		        remove();
		        TimeLineMaker(date);
		        }

			}
        	
        });
        yesterday.setOnClickListener(new OnClickListener(){

			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub
				calendar.add(Calendar.DATE, -1);
				year = String.valueOf(calendar.get(Calendar.YEAR));
		        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
		        day = String.valueOf(calendar.get(Calendar.DATE));
		        mYear   =	calendar.get(Calendar.YEAR);
		        mMonth  =	calendar.get(Calendar.MONTH)+1;
		        mDay 	=	calendar.get(Calendar.DATE);
	        if(month.length()<2)
		        	month = "0" + month;
		        if(day.length()<2)
		        	day = "0" + day;
		        date = year+month+day;
		        mDateDisplay.setText(day);
		        mYearDisplay.setText(year);
		        mMonthDisplay.setText(month);
		        remove();
		        TimeLineMaker(date);
			}
        });
        // display the current date
        
        // add a click listener to the button
        mPickDate.setOnClickListener(new OnClickListener() {
            @SuppressWarnings("deprecation")
			public void onClick(View v) {
                showDialog(DATE_DIALOG_ID);
            }
        });
        calendar.add(Calendar.DATE, -1);
        year = String.valueOf(calendar.get(Calendar.YEAR));
        month = String.valueOf(calendar.get(Calendar.MONTH)+1);
        day = String.valueOf(calendar.get(Calendar.DATE));
        mYear   =	calendar.get(Calendar.YEAR);
        mMonth  =	calendar.get(Calendar.MONTH)+1;
        mDay 	=	calendar.get(Calendar.DATE);

        
        
        if(month.length()<2)
        	month = "0" + month;
        if(day.length()<2)
        	day = "0" + day;
        date = year+month+day;
        YTD_Cur = date;
        mDateDisplay.setText(day);
        mYearDisplay.setText(year);
        mMonthDisplay.setText(month);
 //       arraylist = new ArrayList<TimeLineItem>(); 
        isStart = true;
 //       adapter = new TimeLineNewsAdapter(this, getTaskId(), arraylist);
        //adapter = new ArrayAdapter<String>(this,android.R.layout.simple_list_item_1,arraylist);
        
        adapter = new ArrayAdapter<String>(this,R.layout.timeline_item,arraylist);
		list.setAdapter(adapter);
		list.setOnItemClickListener(new OnItemClickListener(){

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Intent detail_intent = new Intent(getApplication(),FullNews.class);
	            detail_intent.putExtra("news_url", sample);
	            
	            detail_intent.putExtra("position", count_array.get(position));
	            try{
	            startActivity(detail_intent);
	            }
	            catch(Exception e){
	            	Toast.makeText(getApplication(), e.toString() + " intent 에러", Toast.LENGTH_LONG).show();
	            }
			}
			
		});
     // capture our View elements
 
        // add a click listener to the button


        // display the current date

    }
   
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
        case DATE_DIALOG_ID:
            return new DatePickerDialog(this,
                        mDateSetListener,
                        mYear, mMonth-1, mDay);
        }
        return null;
    }
   
    // updates the date we display in the TextView
    private void updateDateDisplay() {
    	if(month.length()<2)
    		month = "0" + month;
    	if(day.length()<2)
    		day = "0" + day;
        date = year+month+day;
        if(date.compareTo(YTD_Cur) > 0){
    		calendar.add(Calendar.DATE, -1);
        }
        else{
        	mDateDisplay.setText(day);
	        mYearDisplay.setText(year);
	        mMonthDisplay.setText(month);
        	calendar.set(mYear, mMonth, mDay);
        	remove();
        	TimeLineMaker(date);
        }

    }
   
    // updates the time we display in the TextView
   private void remove(){
	   if(!arraylist.isEmpty()){
//		   arraylist = new ArrayList<TimeLineItem>();
		   arraylist.removeAll(arraylist);
	   }
	   
//	   adapter = new TimeLineNewsAdapter(this, getTaskId(), arraylist);
//		list.setAdapter(adapter);
//	   adapter.notifyDataSetChanged();
	   count_array.clear();
   }
    // the callback received when the user "sets" the date in the dialog
    private DatePickerDialog.OnDateSetListener mDateSetListener =
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int sub_year,
                                      int monthOfYear, int dayOfMonth) {
                    mYear = sub_year;
                    year = String.valueOf(sub_year);
                    mMonth = monthOfYear;
                    month = String.valueOf(monthOfYear+1);
                    mDay = dayOfMonth;
                    day = String.valueOf(dayOfMonth);
                    
                    updateDateDisplay();
                }
            };
           
         // the callback received when the user "sets" the time in the dialog

     private ArrayList<String> TimeLineMaker(String date){
    	String key = "";
    	String news = "";
    	String url_info = "";
        String line;
 		String page ="";
 		String group1 = "http://json.core.today/json2/?n=50&s=-1";
 		String time1 = "&d=";
        sample = group1 + time1 + date;
        
         URL url;
         HttpURLConnection urlConnection;
         BufferedReader bufreader;
         try {

 		url = new URL(sample + "&sl=1");
 		
 		urlConnection = (HttpURLConnection) url.openConnection();

 		bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
 		while((line=bufreader.readLine())!=null){
 			page+=line;
 		}

 		JSONObject test;
 		JSONArray alljson = new JSONArray(page);
 		ArrayList<Integer> arr = new ArrayList<Integer>();
 		for(int i =0 ; i<24 ; i++)
 			arr.add(0);
 		JSONArray time_frequency;
 			try{	
 				for(int k =0 ; k<50 ; k++){
 		
 					test=new JSONObject(alljson.getString(k));
 					
 					time_frequency = new JSONArray(test.getString("t"));
 					
 					for(int index =0 ; index < 24; index++){
 						int frequency = (int)time_frequency.getDouble(index);
 					
 						if(arr.get(index) < frequency)
 							arr.set(index, frequency);
 					}
 				}
 			}
 			catch(Exception e){

 			}
 		for(int i =0; i < 24 ; i++){
 			test = new JSONObject(alljson.getString(arr.get(i)));
 			count_array.add(arr.get(i));
 			key = test.getString("ti");
// 			arraylist.add(new TimeLineItem(key,sample));
 			arraylist.add(key);
 		}
// 		if(isStart){
// 			adapter = new TimeLineNewsAdapter(this, getTaskId(), arraylist);
// 			list.setAdapter(adapter);
// 		}
 			adapter.notifyDataSetChanged();
 		urlConnection.disconnect();
 		
 		}
 		catch(Exception e){
// 			remove();
// 			arraylist.add(new TimeLineItem("기사가 없습니다","null"));
 			arraylist.add("기사가 없숩니다.");
 			if(isStart){
// 				adapter.notifyDataSetChanged();
// 				adapter = new TimeLineNewsAdapter(this, getTaskId(), arraylist);
// 				list.setAdapter(adapter);
 			}
 		}
         return arraylist;
     }  
     class TimeLineNewsAdapter extends ArrayAdapter{

    	 
     	private View time_line;
     	private Context mcontext;
     	private LayoutInflater inflate;
     	private ArrayList<TimeLineItem> time_item;
     	private TextView timeline_custom;
 		public TimeLineNewsAdapter(Context context, int resource,
 				ArrayList<TimeLineItem> objects) {
 			super(context, resource);
 			mcontext = context;
 			time_item = objects;
 			// TODO Auto-generated constructor stub
 		}
 		public int getCount(){
 			return time_item.size();
 		}
 		@Override
 		public View getView(int position, View convertView, ViewGroup parent){
 		
 			
 			try{
 			time_line = convertView;				
 			inflate = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 			time_line = inflate.inflate(R.layout.timeline_item, null);
 			TimeLineItem p = time_item.get(position);
 			
 			if(p != null){
 			timeline_custom = (TextView) findViewById(R.id.time_dataItem01);
 			timeline_custom .setText(time_item.get(position).getTitle());
 			}
 			return time_line;
 			}
 			catch(Exception e){
 				Log.e("error" + e.toString(), e.toString()+";;;;");

 				return time_line;
 			}
 		}
 				
     }   
     
        
     
     @Override
 	public boolean onOptionsItemSelected(MenuItem item) {
 		if(android.R.id.home == item.getItemId()){
 			finish();
 		}
 		return super.onOptionsItemSelected(item);
 	}
}