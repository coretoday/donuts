package com.core.today.prism.categoryNews;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

import com.core.today.prism.FullNews;
import com.example.core.today.R;

public class CategoryItem extends Activity{

	String pager = null;
	private String category;
	ArrayList<CategoryItemAdapterHelper> arraylistOfNews;
	CategoryAdapter gridViewCustomAdapter;
	GridView gridView;
	private int get_id;
	private CategoryPageParsing syncCategory;
	private TextView text;
	private String group1;
	private ArrayList<Integer> count_array = new ArrayList<Integer>();
//	private CategoryJsonNewsParsing syncCategoryItems;
	@Override
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		setContentView(R.layout.category_news);
		
		////////////////////////////////////////////////////////////////////////////////
		ActionBar actionBar = getActionBar();
		actionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#e15d52")));
		getActionBar().setHomeButtonEnabled(true);
		int actionBarTitle = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		TextView actionBarTitleView = (TextView)getWindow().findViewById(actionBarTitle);
		Typeface font1 = Typeface.createFromAsset(getAssets(), "THEjunggt120.otf");
		actionBarTitleView.setTypeface(font1);
		getActionBar().setTitle("PRISM");
		getActionBar().setDisplayOptions(ActionBar.DISPLAY_HOME_AS_UP | ActionBar.DISPLAY_SHOW_HOME | ActionBar.DISPLAY_SHOW_TITLE);
		////////////////////////////////////////////////////////////////////////////////
		Intent receivedintent = getIntent();
		get_id = receivedintent.getExtras().getInt("category");
		if(get_id == R.id.ctg_economy)
			category = "경제";
		else if(get_id == R.id.ctg_entertain)
			category = "연예";
		else if(get_id == R.id.ctg_global)
			category = "세계";
		else if(get_id == R.id.ctg_it)
			category = "IT";
		else if(get_id == R.id.ctg_main)
			category = "실시간";
		else if(get_id == R.id.ctg_politics)
			category = "정치/사회";
		else if(get_id == R.id.ctg_sports)
			category = "스포츠";
		
		text = (TextView)findViewById(R.id.ctg);
		text.setText(category);
		gridView = (GridView) findViewById(R.id.gridView);
/*		gridViewCustomAdapter = new CategoryAdapter(this, get_id, null);
		gridView.setAdapter(gridViewCustomAdapter);
*/
/*
		try {
			CategoryMaker();
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplicationContext(), e1.toString(), Toast.LENGTH_LONG).show();
		}		
		gridViewCustomAdapter = new CategoryAdapter(this, get_id, arraylistOfNews);
		gridViewCustomAdapter.getCount(10);
		gridView.setAdapter(gridViewCustomAdapter);
*/		
//		final Runnable parser = new CategoryThread(category, get_id, getApplication());
//		Thread parsing = new Thread(parser);
		
		
//		Thread th = new Thread();
		
//		th.run();
/*
		try {
			parsing.join();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			Toast.makeText(getApplication(), e.toString(), Toast.LENGTH_LONG).show();
		}
*/	/*
		arraylistOfNews= ((CategoryThread) parser).getItems();
		gridViewCustomAdapter = new CategoryAdapter(getBaseContext(), get_id, arraylistOfNews);
		gridView.setAdapter(gridViewCustomAdapter);
	*/	
//		arraylistOfNews= ((CategoryThread) parser).getItems();
//		gridViewCustomAdapter = new CategoryAdapter(getBaseContext(), get_id, arraylistOfNews);
//		gridView.setAdapter(gridViewCustomAdapter);
//		gridView = (GridView) findViewById(R.id.gridView);
		syncCategory = new CategoryPageParsing(category,this);
		syncCategory.execute();
	/*	for(int i =0 ; i < 10 ; i ++){
			syncCategoryItems = new CategoryJsonNewsParsing(i,this,syncCategory.getPage());
			syncCategoryItems.execute();
		}
	*/	
		
		gridView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
//				arraylistOfNews = ((CategoryThread) parser).getItems();
				Intent detail_intent = new Intent(getApplication(),FullNews.class);
	            detail_intent.putExtra("news_url", group1+"&n=20");
	            
	            detail_intent.putExtra("position", count_array.get(position));
	            try{
	            startActivity(detail_intent);
	            }
	            catch(Exception e){
//	            	Toast.makeText(getApplication(), e.toString() + " intent ����", Toast.LENGTH_LONG).show();
	            }
			}
		});
		
	}	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if(android.R.id.home == item.getItemId()){
			finish();
		}
		return super.onOptionsItemSelected(item);
	}
	public class CategoryPageParsing extends AsyncTask<Void, Void, Void>{
		private String page = "";
		private String category;
		CategoryAdapter categoryNewsAdapter;
		Activity act;
		CategoryPageParsing(String category, Activity th){
			this.category = category;
			act = th;
		}

		protected void onPreExecute() {
			super.onPreExecute();
		//	Toast.makeText(getApplication(), "ī�װ? : " + category, Toast.LENGTH_LONG).show();
		}

		protected Void doInBackground(Void... params) {

			try {
				Thread.sleep(2000);
			} catch (InterruptedException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
				
			// TODO Auto-generated method stub
			String line = null;
			try {
				group1 = "http://json.core.today/json2/?s=-1&c="+URLEncoder.encode(category, "UTF-8");
			} catch (UnsupportedEncodingException e2) {
				// TODO Auto-generated catch block
	//			Toast.makeText(getApplication(), e2.toString()+ " URl �޴°ſ��� �������...", Toast.LENGTH_LONG).show();
			}

			String sample = group1 + "&n=20&sl=1";

			URL url;
			HttpURLConnection urlConnection;
			BufferedReader bufreader;
			try {
				url = new URL(sample);
				urlConnection = (HttpURLConnection) url.openConnection();
				bufreader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream(),"UTF-8"));
				while((line=bufreader.readLine())!=null){
					page+=line;
				}
				urlConnection.disconnect();
			}
			catch(Exception e){
				Log.e(e.toString(),"error in http.");
			}
			
/////////////////////////////////////////////////////////////////////////////////////			
					
			JSONObject test = null ;         
			arraylistOfNews = new ArrayList<CategoryItemAdapterHelper>();
			JSONArray alljson = null;
			
			try {
				alljson = new JSONArray(page);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				Log.e(e1.toString(),"�ù�1");
			}
			String keyword = ".";
			String image;
			String title = ".";
    		Bitmap bmImg = null;		
			for(int i =0 ; i <10 ; i++){
				try{
					test=new JSONObject(alljson.getString(i));
					count_array.add(i);
				}
				catch(Exception e){
					Log.e(e.toString(),"sb");
				}
				try{
					JSONArray keyw = new JSONArray(test.getString("a"));
					//    if(keyw.getString(0) != "[]" && test.getString("nm") != "[]"){
					//JSONArray arti = new JSONArray(test.getString("nm"));
					keyword=test.getString("k");
					title = test.getString("ti");
					JSONObject URLParser = new JSONObject(keyw.getString(0));

					image = test.getString("i");
					if(image != "null"){
				        
			        	try{
			        		Log.d("error", "sb2");
			        		URL articleURL = null;
			        		articleURL = new URL(image);
			        		HttpURLConnection conn = (HttpURLConnection)articleURL.openConnection();   
			        		conn.setDoInput(true);   
			        		conn.connect();   
			        		InputStream is = conn.getInputStream(); 

			        		bmImg = BitmapFactory.decodeStream(is);
			        		conn.disconnect();
			        		
			        		
			        	}
			        	catch(Exception e){
			        		Log.e("error2",e.toString()+ "bm����"); 

			        	}        
			        
					arraylistOfNews.add(new CategoryItemAdapterHelper(title, keyword, image, URLParser.getString("url"), bmImg));
					}
					else{
					arraylistOfNews.add(new CategoryItemAdapterHelper(title, keyword, image, URLParser.getString("url")));
					}
				}
				catch(Exception e){
					Log.e(e.toString(),"sb3");
				}
			}
			
/////////////////////////////////////////////////////////////////////////////////////////////////			
			return null;				
		}
		protected void onPostExecute(Void result) {
			super.onPostExecute(result);
			try{
				gridViewCustomAdapter = new CategoryAdapter(act,get_id,arraylistOfNews);
				gridView.setAdapter(gridViewCustomAdapter);
//			syncCategoryItems = new CategoryJsonNewsParsing(act,page);
//			syncCategoryItems.execute();
			}
			catch(Exception e){
				Log.e(e.toString(),"�����忡 �������ִ�.");
			}
		}
		protected void onCancelled() {
			super.onCancelled();
		}
			
	}
	/*
	public class CategoryJsonNewsParsing extends AsyncTask<Void, Void, Void>{

	//	private int itemPosition;
		Activity act;
		String pag;
		CategoryJsonNewsParsing(Activity ac, String page){
	//		itemPosition = position;
			act = ac;
			pag = page;
		}
		protected void onPreExecute() {
			super.onPreExecute();
		}
		protected Void doInBackground(Void... params) {
			// TODO Auto-generated method stub
			JSONObject test = null ;         
			arraylistOfNews = new ArrayList<CategoryItemAdapterHelper>();
			JSONArray alljson = null;
			
			try {
				alljson = new JSONArray(pag);
			} catch (JSONException e1) {
				// TODO Auto-generated catch block
				Log.e(e1.toString(),"�ù�1");
			}
			String keyword = ".";
			String image;
			String title;
			
			for(int i =0 ; i <10 ; i++){
				try{
					test=new JSONObject(alljson.getString(i));
				}
				catch(Exception e){
					Log.e(e.toString(),"�ù�2");
				}
				try{
					JSONArray keyw = new JSONArray(test.getString("a"));
					//    if(keyw.getString(0) != "[]" && test.getString("nm") != "[]"){
					//JSONArray arti = new JSONArray(test.getString("nm"));
					keyword=test.getString("k");
					JSONObject URLParser = new JSONObject(keyw.getString(0));
					title = test.getString("ti");
					image = test.getString("i");
					arraylistOfNews.add(new CategoryItemAdapterHelper(title, keyword, image, URLParser.getString("url")), bmimg);
				}
				catch(Exception e){
					Log.e(e.toString(),"�ù�3");
				}
				try{
					if(i == 0){
						gridViewCustomAdapter = new CategoryAdapter(act,get_id,arraylistOfNews);
						gridView.setAdapter(gridViewCustomAdapter);
					}
					else{
						gridViewCustomAdapter.notifyDataSetChanged();
					}
				
				
				}
				catch(Exception e){
					Log.e(e.toString(),"��� �Ľ̿��� �������ִ�.");
				}
					
			}
				return null;
		}
		protected void onPostExecute(Void result){
			super.onPostExecute(result);
	/*		try{
				
			if(itemPosition ==0){
				gridViewCustomAdapter = new CategoryAdapter(act,get_id,arraylistOfNews);
				gridViewCustomAdapter.setItemsNum(0);
				gridView.setAdapter(gridViewCustomAdapter);
				syncCategoryItems.notify();
			}
			else{
				gridViewCustomAdapter.setItemsNum(itemPosition);
				gridViewCustomAdapter.notifyDataSetChanged();
				syncCategoryItems.notify();
			}
			}
			catch(Exception e){
				Log.e("errororroor","������ ã�ƶ�.");
			
		}
	}
	*/
}